const tagH1 = document.getElementsByTagName('h1') // [Objek]
const classTitle = document.getElementsByClassName('title') // [Objek]
const idFirst = document.getElementById('first-title') // Objek

// Query Selector 
const selectByTag = document.querySelector('h1') // Objek
const selectByClass = document.querySelector('.title') // Objek
const selectById = document.querySelector('#first-title') // Objek

// Query Selector All

const selectAllByTag = document.querySelectorAll('h1') // [Objek]
const selectAllByClass = document.querySelectorAll('.title') // [Objek]
const selectAllById = document.querySelectorAll('#first-title') // [Objek]

// Manipulasi Attribut
const divKosong = document.querySelector('.kosong')
const divKosong1 = document.querySelector('.kosong1')
classTitle[0].className = 'red'
selectById.className = 'blue'

// Ubah Atribut dengan key value objek
const redColor = document.getElementsByClassName('red')[0]
redColor.style.color = 'blue'
redColor.style.backgroundColor = 'pink'

// Ubah Atribut dengan Method setAttribute
tagH1[2].setAttribute('class', 'red')
tagH1[2].setAttribute('style', 'background-color : blue;')


// Ubah Isi Didalam TAG
tagH1[3].innerText = 'INI TEKS KOSONG'
tagH1[1].textContent = 'INI TEXT CONTENT'
divKosong.innerHTML = '<h1> Ini Tag Baru </h1>'
divKosong1.innerText = '<h1> Ini Tag Baru </h1>'

const list = document.querySelector('ul')

list.innerHTML = `
<li>x</li>
<li>y</li>
<li>z</li>
`
const childOfList = list.children

childOfList[0].innerHTML ='' 



list.removeChild(childOfList[1])

// const lists = document.querySelectorAll('li')
// console.log(lists)
// list.removeChild(lists[2])

divKosong.innerHTML = ''



// selectById.addEventListener('click', () => {
//     selectById.style.backgroundColor = 'red'
// } )

// Event






