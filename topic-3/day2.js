// Berisi Player dari GAME
class Game {
    // Player 1 & 2  { choosen :  }
    constructor(player1, player2) {
        this.player1 = player1
        this.player2 = player2
        this.winner 
        this.isCanPlay = false
    }

    getRandomValue() {
        const num = Math.floor(Math.random() * 3);
        let choosen = ''
        if(num === 0) {
            choosen = 'gunting'
            cardGuntingCom.changeBackgroundColor()
        }else if(num === 1) {
            choosen = 'batu'
            cardBatuCom.changeBackgroundColor()
        }else if(num === 2) {
            choosen = 'kertas'
            cardKertasCom.changeBackgroundColor()
        }
        this.player2.choosen = choosen
        this.checkResult()
        console.log(this.player1, "<<< INI PLAYER 1")
        console.log(this.player2, "<< INI PLAYER 2")
    }


    checkResult() {
        let choosenP1 = this.player1.choosen
        let choosenP2 = this.player2.choosen
        console.log('==== check =====')
        this.winner = this.player1
        console.log(resultDiv)
        resultDiv.innerHTML = 
        `
         <h1>${this.winner.name} </h1>
        `
        if(choosenP1 === 'kertas' && choosenP2 === 'batu') {
           
            // P1 WINNER
        }

    }




} 

class Card {
    constructor (type, isChoosen, element, isClick) {
        this.type = type
        this.isChoosen = isChoosen
        this.element = element
        this.isClick = isClick
        if(this.isClick) {
            this.element.addEventListener('click', () => {
                this.changeBackgroundColor()
                game.player1.choosen = this.type
                game.getRandomValue()
            })
        }


    }

    changeBackgroundColor() {
        let color = ''
        console.log(this.isClick)
        if(this.isClick) {
            color = 'yellow'
        }else {
            color = 'green'
        }
        this.element.style.backgroundColor = color
    }

    removeEventListener() {
        console.log(' card di hapus ')
        this.element.removeEventListener('click' , () => {
            console.log('delete event')
        })
    }

    

} 





// Ini untuk GAME FLOW

const inputName = document.getElementById('inputName')
const submitButton = document.getElementById('submitButton')
const guntingP1 = document.getElementById('gunting-p1')
const batuP1 = document.getElementById('batu-p1')
const kertasP1 = document.getElementById('kertas-p1')
const guntingCom = document.getElementById('gunting-com')
const batuCom = document.getElementById('batu-com')
const kertasCom= document.getElementById('kertas-com')
const resultDiv = document.querySelector('.result')
const playButton = document.getElementById('playButton')

const p1 = {
    name : 'Player 1',
    choosen : ''
}
const p2 = {
    name : 'Player 2',
    choosen : ''
}
const game = new Game(p1, p2)

let cardBatuP1
let cardKertasP1
let cardGuntingCom
let cardBatuCom
let cardKertasCom



playButton.addEventListener('click', () => {
    console.log(game.isCanPlay)
    game.isCanPlay = !game.isCanPlay
    if(game.isCanPlay) {
        console.log('=== masuk ====')
        cardGuntingP1 = new Card('gunting', false, guntingP1, true)
        cardBatuP1 = new Card('batu', false, batuP1, true)
        cardKertasP1 = new Card('kertas', false, kertasP1, true)
        cardGuntingCom = new Card('gunting', false, guntingCom)
        cardBatuCom = new Card('batu', false, batuCom)
        cardKertasCom = new Card('kertas', false, kertasCom)
    }
    
    // if(game.isCanPlay) {
    //     game.isCanPlay = false
    // }else {
    //     game.isCanPlay = true
    // }
})



// tombol replay  => reset pilihan


