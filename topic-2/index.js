/**
 Class -> Blue print untuk sebuah objek, dimana objek kumpulan entitas, kelompok benda.

 Mobil => ada pintu, ada ban, ada lampu , ada spion => Properti.
 Mobil => Bergerak/ Berjalan. Nyalakan Lampu, bunyikan klakson => Method. 

*/

const Car0 = {
    tire : 0,
    door : 3,
    lamp: 4,
    spion : 2,

    walk : () => {
        console.log('Walk')
    },
    turnOnLamp : () => {
        console.log("Lamp")
    }
}


const Car2 = {
    tire : 3,
    door : 4,
    lamp: 5,
    spion : 2,

    walk : () => {
        console.log('Walk')
    },
    turnOnLamp : () => {
        console.log("Lamp")
    }
}

const Car3 = {
    tire : 3,
    door : 4,
    lamp: 5,
    spion : 2,

    walk : () => {
        console.log('Walk')
    },
    turnOnLamp : () => {
        console.log("Lamp")
    }
}

class Car {
    #tank

    constructor(tire = 4, door = 2, lamp = 4, spion = 2, name = 'Mobil') {
        // console.log('==== constructor ====')
        this.tire = tire
        this.door = door,
        this.lamp = lamp, 
        this.kacaSpion = spion
        this.name = 'Mobil ' + name
        this.#tank = 0
    }

    async berjalan() {
        try {
            if(this.#tank > 0) {
                this.#tank = this.#tank - 2 
                console.log('berjalan....')
                return 'Berjalannn'
            }else {
                throw 'gk ada bensin'
            }
        } catch (error) {
            return error
        }
    }

    #privateMethod() {
        console.log('=== ini private method ====')
    }

    nyalakanLampu() {
        this.#tank--
        console.log('nyalakan lampu...')
        // this.#privateMethod()
        
        // console.log(this.#tank)
        
    }

    getTankValue() {
        // logic 
        return this.#tank
    }

    setTankValue(value) {
        this.#tank = value
    }

    setDoorValue(value) {
        if(value < 2 ) {
            console.log("Pintu harus lebih dari 1")
        }else {
            this.door = value
        }
    }

    static staticMethod1() {
        console.log('Static method')
    }

}

// Car.staticMethod1()

const yaris = new Car(4, 2, 2,2, 'yaris')
const brio = new Car(4,2,2,2, 'Brio')
const jazz = new Car(4,4,2,2, 'Jazz')
const entah = new Car()

// jazz.door = 8
// console.log(jazz.door)
jazz.setTankValue(8)
const tankOfJazz = jazz.getTankValue() 
console.log(tankOfJazz)
jazz.berjalan()
.then(res => {
    console.log(res, "<<< res")
})
.catch(err => {
    console.log(err)
})
// console.log(jalan)
jazz.nyalakanLampu()
// jazz.setDoorValue(1)
console.log(jazz.getTankValue())






