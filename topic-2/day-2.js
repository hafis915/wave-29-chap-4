// Inheritance Class => Parents & Child v
// Override , overload v
// Encapsulation v

// Abstraction 
// polymorphism. 


class Car {
    #harga
    constructor(tire, door, seat) {
        // console.log(this.constructor, "<<< ini constructor ")
        if(this.constructor === Car) {
            throw new Error('Car is an abstract Class')
        }
        this.tire = tire
        this.door = door
        this.seat = seat
        this.#harga = 80000
        this._year = 2022
    }

    fillTheTank() {}

    speedUp() {
        console.log('==== speed up at superclas ===', this.tire)
    }

    brake() {
        console.log('=== brake at super class')
    }

    startEngine() {}
}

class SportCar extends Car {

    constructor(tire, door, seat) {
        super(tire, door, seat) 

    }

    brake() {
        console.log('===== brake at subclass sportCar =====')
        console.log(this._year)
    }

    speedUp(isNo2) {
        if(isNo2) {
            console.log('=== speed up with No2')
        }else {
            console.log('==== Speed up without No2')
        }
    }
}

class FamilyCar extends Car {
    constructor(tire, door, seat) {
        super(tire, door, seat) 
        
    }
}

class RaceCar extends Car {
    constructor(tire, door, seat) {
        super(tire, door, seat) 

    }
}

const ferrari = new SportCar(4,2,2)
const avanza = new FamilyCar(4,4,6)
const Xenia = new FamilyCar(4,4,6)

ferrari._year // Gk boleh, tapi tidak error. 


// Polymorphism
/**
 Human 
 
 PublicServer

 Military 


 Army, Police  => Military, Public Server

 Doctor => Public Server

 */


 class Human {
    constructor(props) {
        if(this.constructor === Human) {
            throw new Error('Car is an abstract Class')
        }
        this.name = props.name 
        this.address = props.address
    }

    introduce() {
        console.log('=== At human Class ', +  `my name is ${this.name}`)
    }

    work() {
        console.log(this.address, "<<< Human")
        console.log(`work as ${this.constructor.name}`)
    }
 }

 const PublicServer = (Base) => class extends Base {

    // constructor(props ) {
    //     super(props)
    //     console.log(this.constructor, "<< ps")
    //     console.log('=== publice server constructor ===')
    // }

    save() {
        console.log(super.address, "<< publice server")

        console.log("=== at public server ===", '=== save ====')
    }
 }

 const Military = (Base)  => class extends Base {
    shoot() {
        console.log('==== shoot ====')
    }
 }


 class Doctor extends PublicServer(Human) {
    constructor(props) {
        super(props)
        console.log(props, "<< objek")
        console.log(this.constructor, "<< Doctor")

    }

    work() {
        console.log(super.address)
        super.work() // Ini dari Human
        super.save() // Ini dari Dari Publice Server
    }
 }

 class Police extends PublicServer(Military(Human) ) {
    constructor(props) {
        super(props)
        console.log(this.constructor, "<< police")
        // super(props)
    }

    work() {
        super.shoot()
        super.work()
        super.save()
    }
 }


const boyke = new Doctor({
    name : 'Boyke',
    address : 'apalah'
})

// const Prabowo = new Police({
//     name : 'Prabowo',
//     address : 'itulah'
// })

// Prabowo.work()
// boyke.work()