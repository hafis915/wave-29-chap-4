// console.log('Hello World')



/**
- variabel di definisikan, 
- assign value(memberikan nilai ke variabel)

Perbedaan dalam definisi dan assign: 

- let & var
    - let & var bisa di reassign
    - var bisa redefine let x
    - assign value bisa di pending
- const
    - x assign belakangan

Perbedaan dalam scope: 
- var

- let dan const

Tipe data : 
    - primitif => string, integer, boolean, null , undifined
    - Non Primitif =>  array & objek


    String : 
        - builtin function
        - indexing
*/
// String

let  str1 = 'nama'
let str2 = 'name2'
let str3 = str1 * str2
// Integer  => 2 
// Float => 2.0

let int = 58.5
let strInt = '88'
// strInt = Number(strInt)






// Boolean => true , false

let isTall = true
isTall = false

// Operator perbandingan
/**
 >, < , == ,===, != ,!==, >= , <= 

// Logical Operator
 && (and) || (or)

*/

// const int1 = 3
// const int2 = 4 

// const bool1 = 

// console.log(bool1)

// truthy dan falsy
/**
 tipe data | truthy | falsy
-----------------------------
 String    | 'dawd ' | ''
------------------------------
 Integer   | 2121   | 0
 Boolean   | true   | false
 undifined |        | v
 null      |        | v

 */

let name = 'dawdw'
let int2 = 0

// if(name && int2) {
//     console.log('=== true ==')
// }else {
//     console.log('=== false ==')
// }

// array & Object  => wadah yang menampung lebih dari 1 tipe data. 

// Array => []

const arr1 = [1,2,3,4]


for(let i = 0 ; i < arr1.length ; i++ ) {
    
    if( arr1[i] < 2) {
        arr1[i] = 3
    }
    // console.log(arr1[i])
}

// console.log(arr1)


// pass by reference
// const arr2 = arr1
// arr1[3] = 8

// Dapat ukuran array => length
// console.log(arr1.length)


//Object {}

const obj = {
    firstName : 'hafis',
    age : 89,
}

for(let key in obj) {

    if(key === 'age') {
        obj[key] = 90
    }
}

console.log(obj)

// const obj2 = obj
// obj.age = 70

// pass by value

// let address = 'makassar'
// let address2 = address


// address = 'jakarta'














