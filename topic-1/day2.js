
// const hasil = penjumlahan(2,3)
// console.log(hasil)


function penjumlahan(num1, num2 ) {
    const result = num1 + num2
    console.log(result, "<< penjumlahan")
    return result
}

const jumlahX = 6
const jumlah =  penjumlahan(4,2) // ada valuenya => 6
// console.log(jumlah)
// console.log(jumlahX)



const pengurangan = (num1, num2) => {
    const result = num1 - num2
    for(let i = 0 ; i < 4 ; i++) {
        console.log(i)
    } 
    console.log('==== pengurangan selesai ====')
}

// penjumlahan(8,2) 
// pengurangan(9,1)
// High Order Function

// Callback
function callBackFunction(f) {

    const result = f(8,2)
    // return f(8,3)
    return result
}
const kurang = callBackFunction((num1, num2) => {
    const result = num1 - num2
    return result
})

// const jumlah =  callBackFunction(penjumlahan)

//Promise Function
// Asyncronous Function

function promiseFunction(par1, par2) {

    return new Promise((resolve,reject) => {
        if(par1 < par2) {
            // console.log('=== promise function ====')

            const result= par1 + par2
            resolve(result)
        }else if(par1 === par2) {
            resolve(1)
        }
        else {
            reject('par1 harus > par2')
        }
    })
}


async function asyncFunction(par1, par2) {
    try {
        if(par1 < par2) {
            for(let i = 0 ; i < 4 ; i++) {
                console.log(i, "<< ASYNC")
            } 
            let result2 = await promiseFunction(1,3)
            console.log("<< PROMISE >>>")
            return par1 + par2
        }else {
            throw 'error'
        }
    } catch (error) {
        return error
    }
}
// asyncFunction(1,2)
// pengurangan(3,4)
// penjumlahan(4,3)

//closure
function outerFunction(count) {

    function plusFunction() {
        count = count + 1
        return count
    }

    function minFunction() {
        count = count -1
        return count
    }


    return {
        add : plusFunction,
        min : minFunction
    }
}

function isiLemari(kelompokBaju) {

    function kurangiBaju() {
        kelompokBaju.pop()
        return kelompokBaju
    }

    function addBaju(bajuBaru){
        kelompokBaju.push(bajuBaru)
        return kelompokBaju
    }

    function tampilkanBaju(){
        return kelompokBaju
    }

    return {
        tambahkanBaju : addBaju,
        kurangiBaju : kurangiBaju,
        cekLemari : tampilkanBaju
    }
}

const lemari1 = isiLemari(['celana', 'baju olahraga'])
const lemari2 = isiLemari(['adwa', 'adawd'])



// asyncFunction(1,2)
// .then(res => {
//     console.log(res)
// })
// .catch(err => {
//     console.log(err)
// })
